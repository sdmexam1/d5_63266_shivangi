const express = require('express')
const app = express()
app.use(express.json())

const employeeRouter = require('./routes/employee.js')

app.use(employeeRouter)
app.listen(4000, '0.0.0.0', () => {
  console.log('server is started on port 4000')
})
