const mysql = require('mysql2')

const Pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'manager',
  database: 'examDB',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 20,
})

module.exports = {
  Pool,
}
