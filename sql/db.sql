-- Q1.Containerize MySQL and create Employee(emp_id, emp_Name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate) table inside that. Employee table will be created through db.sql file.

-- Now create server side application using Node and express and Perform following application 

-- 1. GET  --> Display Employee using name from MySQL

-- 2. POST --> ADD Employee data into  MySQL table

-- 3. UPDATE --> Update emp_Address ,emp_Email and emp_MobileNo into  MySQL table

-- 4. DELETE --> Delete Employee from  MySQL

-- Need to Upload data on Git.

-- 1.You need to upload db.sql, Mysql DockerFile,Node DockerFile and Node Application 

-- 2.You need take Screenshot of Postman window (Successfully excuted All APIs) and upload on git. 

create table Employee(
    emp_id int primary key ,
    emp_Name Varchar(50),
    emp_Address Varchar(50),
    emp_Email Varchar(50),
    emp_MobileNo int ,
    emp_DOB Varchar(10) ,
    emp_JoiningDate Varchar(10)
    )