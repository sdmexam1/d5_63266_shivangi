const express = require('express')
const utils = require('../utils')
const db = require('../db')
const router = express.Router()
router.post('/employee', (req, res) => {
  const {
    emp_id,
    emp_Name,
    emp_Address,
    emp_Email,
    emp_MobileNo,
    emp_DOB,
    emp_JoiningDate,
  } = req.body
  console.log(emp_id)
  const statement = `insert into Employee( emp_id, emp_Name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate) values(?,?,?,?.?,?,?);`
  db.Pool.query(
    statement,
    [
      emp_id,
      emp_Name,
      emp_Address,
      emp_Email,
      emp_MobileNo,
      emp_DOB,
      emp_JoiningDate,
    ],
    (err, data) => {
      res.send(utils.createResult(err, data))
    }
  )
})

module.exports = router

router.get('/getallemployee', (req, res) => {
  const statement = `select * from Employee`
  db.Pool.query(statement, (err, data) => {
    res.send(utils.createResult(err, data))
  })
})
router.put('/:emp_id', (req, res) => {
  const { emp_id } = req.params
  const {
    emp_Name,
    emp_Address,
    emp_Email,
    emp_MobileNo,
    emp_DOB,
    emp_JoiningDate,
  } = req.body
  const statement = `update Employee set emp_Name=?, emp_Address=?,emp_Email=?,emp_MobileNo=?,emp_DOB=?, emp_JoiningDate=? where emp_id=?;`
  db.Pool.query(
    statement,
    [
      emp_Name,
      emp_Address,
      emp_Email,
      emp_MobileNo,
      emp_DOB,
      emp_JoiningDate,
      emp_id,
    ],
    (err, data) => {
      res.send(utils.createResult(err, data))
    }
  )
})

router.delete('/removeEmployee/:emp_id', (req, res) => {
  const { emp_id } = req.params
  const statement = `delete from Employee where emp_id=?;`
  db.Pool.query(statement, [emp_id], (err, data) => {
    res.send(utils.createResult(err, data))
  })
})
